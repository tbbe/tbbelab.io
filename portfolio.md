---
layout: page
title: Portfolio
comments: false
---

Jeg er en teknisk tester med speciale i testautomatisering samt test arkitektur.
Jeg vil meget gerne hjælpe med at finde de rigtige værktøjer til at forestå en eventuel automatisering af din regressionstest, systemtest eller integrationstest. Jeg vil også gerne hjælpe med at udføre selve automatiseringen, og jeg vil også gerne hjælpe med at udlede testcases fra krav og use cases.
Jeg er ISTQB Advanced Technical Test Analyst certificeret.


Jeg er vant til at arbejde i en agil kontekst og det er også min foretrukne projektform. Det betyder ikke jeg ikke kan færdes i de lidt mere traditionelle projektstyreformer, men hvis jeg selv kan vælge, er det agilt jeg går med.
Jeg er certificeret Scrum master.


Før jeg blev tester har jeg arbejdet med udvikling og arkitektur på JVM platformen samt på SAP NetWeaver platformen. Jeg er certificeret i begge platforme.

# Projekterfaring

![Erhvers og Selskabsstyrelsen](/assets/img/erst.png){: style="float: right"}

### ERST DECEMBER 2021 - NU

Testopgave hos Erhvervs og Selskabsstyrelsen som overvejende handler om at test interne web applikationer med Geb og Spock frameworks. Dejligt nørdet og lige ud af landevejen. Der vil naturligvis også være noget arbejde i form af specifikation og defination af de omkringliggende opgaver, som f.eks testdata tilvejebringelse på en GDPR sikker måde.

![Velliv](/assets/img/velliv.png){: style="float: right"}

### VELLIV OKTOBER 2021 - DECEMBER 2021
Testopgave der handler om at hjælpe med testdesign og testafvikling af forskellige testopgaver som primært beskæftiger sig med test af SOAP webservices.

![Topdanmark >](/assets/img/topdanmark.png){: style="float: right"}

### TOPDANMARK OKTOBER 2018 - OKTOBER 2021
Testmanagement opgave med teknisk overbygning. Det er mit job at facilitere den tekniske side af integrationstesten af systemet under test sådan at kundens testteams kan udføre testen. Derudover test af relevante skatte indberetninger samt support og daglig arbejder med testmiljøer og tilvejebringelse og administration af testdata.

![SKAT >](/assets/img/skat.png){: style="float: right"}

### SKAT JANUAR 2017 - JUNI 2018
Test af indleveringssystemer for dels OPEC landende og dels for Danmark. Her anvendes primært Cucumber og Spock til at forestå den automatiserede test. Selve projekterne er drevet agilt med Scrum modellen.

![DSB >](/assets/img/dsb.png){: style="float: right"}

### DSB IT AUGUST 2011 – JANUAR 2017
Test arkitekt samt teknisk tester for nyt egetudviklet eCommerce system til DSB. Derudover test af DSB’s netbutik version 6. Projekterne er drevet agilt med en modificeret version af Scrum modellen.

<p>&nbsp;</p>

![PFA >](/assets/img/pfa.png){: style="float: right"}

### PFA PENSION A/S MARTS 2010 – AUGUST 2011
Arbejde med teknisk arkitektur samt kvalitetsforbedring af den eksisterende kodebase.

![ASE >](/assets/img/ase.png){: style="float: right"}

### ASE FEBRUAR 2010
Arbejder med forsk. Java teknologier til applikations integration.
	Dette var en meget kort ansættelse da IT chefen valgte at forlade virksomheden umiddelbart efter min ansættelse.

![FOSS >](/assets/img/foss.png){: style="float: right"}

### FOSS ELECTRIC JUNI 2009 – JUNI 2009
Arbejdede som ABAP udvikler med at integrere deres HCM installation med deres AD (Active Directory).


![Dong >](/assets/img/dong.png){: style="float: right"}

### DONG ENERGY MAJ 2009 – JUNI 2009
Arbejdede som Portal konsulent med at implementere nyt NWDI samt overførsel af eksisterende projekter til det nyoprettede NWDI samt samling af dokumentation og standarder i portalrolle.

![CPH >](/assets/img/cph.png){: style="float: right"}

### KØBENHAVNS LUFTHAVNE DECEMBER 2008
Arbejde som Java udvikler og portal administrator med fejlfinding i diverse egenudviklede applikationer ifm. opgradering fra NetWeaver Portal 6.40 til 7.0

![BAT >](/assets/img/bat.png){: style="float: right"}

### BRITISH AMERICAN TOBACO NOVEMBER 2008 – DECEMBER 2008
Arbejdede som ABAP udvikler med at tilrette og videreudvikling af eksisterende masterdata applikation

![Dong >](/assets/img/dong.png){: style="float: right"}

### DONG ENERGY OKTOBER 2008 – JUNI 2008
Arbejdede som ABAP udvikler med implementering af CRM 2007 hos marketingsafdelingen for B2B samt integration af eksisterende egenudviklet mini CRM system.

![Applicon >](/assets/img/applicon.png){: style="float: right"}

### APPLICON A/S AUGUST 2008 – JUNI 2009
Arbejdede som portal udvikler og arkitekt med design og implementation af CV styrings applikation

![Dong >](/assets/img/dong.png){: style="float: right"}

### DONG ENERGY APRIL 2008 – AUGUST 2008
Arbejdede som Java udvikler samt portal administrator med 1-1 konvertering og opgradering af egenudviklet mini CRM applikation fra HTMLB for Java til Web Dynpro for Java.

![BAT >](/assets/img/bat.png){: style="float: right"}

### SKANDINAVISK TOBAKSKOMPAGNI FEBRUAR 2008 – OKTOBER 2008
Arbejdede som Java udvikler samt ABAP udvikler for at implementere et Information Access System (IAS) til brug for fremfinding af ustrukturerede data inkl. BI rapporter og MS Office dokumenter. Løsningen er udviklet i Web Dynpro for Java og Web Dynpro for ABAP med integration til SAP TREX.

### SKANDINAVISK TOBAKSKOMPAGNI NOVEMBER 2007 – FABRUAR 2008
Arbejdede som Java udvikler og portal administrator med at implementere en egenudviklet webshop til distribution af merchendise og best practise løsningsbeskrivelser.

![CPH >](/assets/img/cph.png){: style="float: right"}

### KØBENHAVNS LUFTHAVNE NOVEMBER 2007
Arbejdede som Java udvikler med implementation af mini personale planlægger med interface til både administration og slutbrugere. Specielt store krav til brugervenlighed grundet ikke IT vandt brugergruppe.

![Dong >](/assets/img/dong.png){: style="float: right"}

### DONG ENERGY OKTOBER 2007 – NOVEMBER 2007
Arbejdede som Java udvikler og portal administrator med generel ERP portefølje implementation samt udvikling af Web Dynpro Java transaction launchere og implementering af BI / IP navigations struktur.

### DONG ENERGY SEPTEMBER 2007 – APRIL 2008
Arbejdede som Java udvikler og portal konsulent med support og vedligehold af kundens egenudviklede HTMLB for Java baserede mini CRM.

### DONG ENERGY JUNI 2006 – AUGUST 2006
Arbejdede som Java udvikler og Adobe Flex udvikler med udvikling af prototyper til løsning af forskellige applikations integrations opgaver.

### DONG ENERGY MAJ 2006
Arbejdede som Java udvikler med at udvikle portal rapport applikation med integration til PLM og PS samt udvikling af tilhørende portalteam.

![Post >](/assets/img/post.png){: style="float: right"}

### POSTDANMARK JUNI 2005 – FEBRUAR 2006
Arbejdede som Java udvikler og portal administrator med implementation af Enterprise Portal 6.40 med specielt fokus på CRM og Internet Sales samt egenudviklet reklamationssystem med integration til CRM via XI.

### POSTDANMARK FEBRUAR 2004 – FEBRUAR 2006
Arbejdede som Java udvikler og portal administrator med udvikling af external facing portal til eksponering af applikationer på internettet efter overordnet web strategi.

### POST DANMARK FEBRUAR 2004 – FEBRUAR 2006
Arbejdede som Java udvikler med udvikling af buildstruktur for at muliggøre bygge, linke og deploye SAP Java applikationer via Apache Maven.

### POSTDANMARK FEBRUAR 2004 – FEBRUAR 2006
Arbejdede som Java udvikler med at udvikle DynPagePlus, et lightweight udviklings framework til hurtig fremstilling af native portal applikationer baseret på SAP NetWeaver og HTMLB

### POSTDANMARK FEBRUAR 2004 – FEBRUAR 2006
Arbejdede som Java udvikler med at oprette custom ISR (Internal Service Request) i MSS.

### POSTDANMARK DECEMBER 2002 – JUNI 2003
Arbejdede som Java udvikler med implementation af legacy system til styring af resultatløn i distributionen. Udviklet på en BEA WebLogic platform med underliggende Oracle database.

### POSTDANMARK MAJ 2003 – SEPTEMBER 2003
Arbejdede som Java udvikler med implementation af legacy system til styring af tidsregistrering via scanner/kortlæser samt web grænseflade. Udviklet på en BEA WebLogic platform med underliggende Oracle database.

### POSTDANMARK SEPTEMBER 2003 – FEBRUAR 2004
Arbejdede som Java udvikler med implementation af kreditvurderingsapplikation med integration til Dunn & Bradstreet. Udviklet på en BEA WebLogic platform med underliggende Oracle database samt BEATuxedo services til dataudveksling.