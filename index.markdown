---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
![Thomas Bentzen](/assets/img/me.jpg){: style="float: right"}
Denne side er min primære outlet for tanker omkring mit arbejde som software tester. De emner jeg beskæftiger mig mest med er følgende:

- Livet med en iPad som primær computer. Se evt min side om mit [iPad eventyr](/ipadeventyr.html).
- Bullet Journals - jeg kan godt lide at bruge en Bullet Journal, og det skriver jeg så om nogen gange. Det emne har også [sin egen side](/bulletjournal.html).
- Testautomatisering og generel automatisering. Mit job som teknisk tester indeholder meget automatisering, hvorfor jeg er på hjemmebane her.