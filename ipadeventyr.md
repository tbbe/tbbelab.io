---
layout: page
title: IPad eventyr
comments: false
---

> _Jeg er ved at starte et nyt eventyr, og det eventyr går ud på at jeg vil bruge en iPad som min computer. Det kommer ikke til at være det eneste hardware jeg kommer til at bruge, men for mit personlige behov og for mit it behov overfor min arbejdsgiver vil det være de eneste hardware. Jeg kommer også til at bruge denne iPad hos kunderne som notesbog. Hvis du er interesseret så læs med._

# Motivationen

Jeg arbejder som IT konsulent, nærmere specificeret som teknisk tester. Jeg arbejder for konsulent firmaet [Q Nation](https://www.qnation.dk/), men mit rigtige arbejde er hos de kunder der skal bruge min konsulentbistand. Det betyder at når jeg er hos kunderne, så får jeg udleveret noget af deres hardware som jeg skal arbejde på.

I forhold til det arbejde jeg laver som er relateret til Q Nation, har jeg denne iPad som jeg burger. Privat har jeg også brug for et eller andet til at gøre de ting man nu skal gøre som en del af at være dansker. Så mit behov for en personlig computer mener jeg fint kan klares af en iPad. Eller det har jeg i hvert fald tænkt mig at prøve.

## Mit konkrete behov

### Hos kunden

Jeg har behov for at have adgang til Q Nations systemer mens jeg er hos kunden. De kunde projekter jeg deltager i er som regel af flere års varighed hvorfor jeg har behov for at kunne kommunikere med mit firma under vejs. Jeg har også behov for et sted hvor jeg kan opbevare min elektroniske værktøjskasse samt vidensartikler, som jeg bruger som reference materiale og andre opslagsværker. Det er min erfaring at jeg ret sjældent får lov til at installere mit eget software på kundens hardware, hvorfor det er rart at have det på en sekundær maskine.

Det er ofte at kunden ikke stiller et netværk til rådighed, andet end for deres egen hardware, hvorfor det er smart hvis min iPad har sit eget netværk med til disse situationer. Hvis kunden giver mulighed for at bruge deres netværk, i den ene eller den anden form, er det som regel også med en del restriktioner, hvoraf en af den næsten altid er at jeg ikke må tilgå nogen form for e-mail mens jeg bruger deres net.

Jeg har op til nu været en stor bruger af notesbøger, til dels at notere alt lige fra møde referater, en slags dagbog og opgaver i, samt at kunne lave hurtige skitser over de ting jeg har beskæftiget mig med, for bedre at kunne danne mig et overblik over krævende opgaver, eller bare for simpelt at nedbryde en kompliceret problemstilling til fordøjelige bidder. Ved at bruge en iPad forventer jeg at denne del kan være 100% elektronisk. Faktisk er det en målsætning at slippe helt for papir.

### Q Nation

For at opretholde min ansættelse hos Q Nation har de også en del forventninger af mig. Q Nation er et super fantastisk firma at arbejde for, og de forventninger de har til mig, er meget nemme at indfri. Det forventes i det store hele bare at jeg vedligeholder mit CV og min tidsregistrering, da de to dele opretholder vores eksistensgrundlag. Derudover arbejder jeg af og til med deres system portefølje og hjemmeside. Ikke ofte, men nogen gange sker det at jeg deltager i udbudsbesvarelser.For at opretholde min ansættelse hos Q Nation har de også en del forventninger af mig. Q Nation er et super fantastisk firma at arbejde for, og de forventninger de har til mig, er meget nemme at indfri. Det forventes i det store hele bare at jeg vedligeholder mit CV og min tidsregistrering, da de to dele opretholder vores eksistensgrundlag. Derudover arbejder jeg af og til med deres system portefølje og hjemmeside. Ikke ofte, men nogen gange sker det at jeg deltager i udbudsbesvarelser.

Intern kommunikation sker via Slack og Google Meet, ekstern via e-mail og over telefon, dog er der tilføjet lidt ekstra komponenter her i Corona tiden så som: Skype, Discord, Zoom og MS Teams. Dette er alle teknologier som kunderne benytter, hvorfor jeg også skal kunne deltage på dem.

### Derhjemme

Hjemme forventer jeg at have en computer som jeg kan bruge til at betjene alle de funktioner jeg normalt ville gøre: surfe, e-mail, sociale medier, læse bøger, høre musik og alt mit skrive arbejde (f.eks det her).

Jeg har et skrive bord, det anskaffede jeg mig for ca 2 år siden, og det har været noget rod lige siden. Og det er til trods for det var en af de mindste skriveborde jeg kunne få på det tidspunkt. Lige som hos kunden, så forventer jeg at jeg kan slippe af med alt papir arkiver. Jeg forventer også at dette ikke må fylde ret meget da jeg ikke har planer om at det skal stå fremme når jeg ikke bruger det.

## Konklusion

Jeg har behov for en computer som kan håndtere følgende:

- Google G-suite brugt af Q Nation som kontor pakke.
- De moderne kommunikations værktøjer som kunderne bruger.
- Skal kunne have min elektroniske værktøjskasse.
- Skal kunne hjælpe mig med at være papirløs, og derved også kunne fungere som notesbog.
- Være kommunikations og underholdnings center for mine interesser.
- Skal have sit eget netværk sådan jeg ikke er afhængig af der er Wi-Fi tilgængeligt når jeg skal bruge den.
- Skal have plads nok til alt dette og også mere så jeg ikke behøver bekymre mig om plads.

Min iPad dækker alle disse behov, i hvert fald på papiret. Jeg tror så meget på det at jeg har valgt at kaste mig ud i det i en 3 års periode. Jeg har et alternativ på plads hvis dette ikke virker, men jeg er ret overbevist om at det kommer det til.

# Opsætning

## iPad

Det er naturligvis en iPad 😀. Da jeg er lidt bange for ikke at have plads nok når jeg skal have alt mit materiale på denne iPad, så har jeg valgt en med masser er plads. Det er 2020 version af iPad pro 12,9” med 1 TB lagerkapacitet. Det betyder også at den har 6GB ram i stedet for 4GB ram. Da iPadOS kører med et meget begrænset baggrunds applikationer og maks 3 applikationer i forgrunden regner jeg med at 6GB ram er meget mere end rigeligt.

Da jeg regner med at skulle skrive en del, har jeg valgt også at købe et magic keyboard. Det har vist sig at være en fantastisk ide. Efter jeg har købt det, er min iPad nu også en funktionel laptop.

Da jeg forventer at bruge min iPad som notesbog, har jeg også købt en 2. Generation Apple Pencil. Jeg er ikke kunstner og jeg kan ikke mærke forskel fra 1. generation til 2. generation, Men det er super fedt at den kan sidde fast i toppen af iPad’en med magneter. Så er der også mindre chance for at jeg smider den væk.

Jeg har derudover et par USB-C dongler – en med USB-A og HDMI sådan jeg kan forbinde en projektor til min IPad i forhold til at kunne præsentere, og en med kun USB-A primært til at importere billeder fra mit kamera.

## Backup

Jeg har en WD MyCloud på 4GB til at have backup på. Den fungere sådan at den skal sættes til et sted hvor der er hurtigt internet, og så forbinder man den med en app og herefter kan den bruges via IPad’ens Arkiver app som et tilsluttet filsystem. Lidt lige som Google Drive, Box og Dropbox. Det er dog ikke en delt ressource, den er kun til mig. Det gør det super nemt at tage backup af de ting jeg gerne vil have taget backup af, jeg kopiere dem bare derover.

Den virker over nettet (AES256 krypteret) så det er nogen lunde sikkert. Hvis jeg skal kopiere store filer, er det naturligvis bedst at være på Wi-Fi med den, da det går super hurtigt der, over nettet er der mange faktorer der spiller ind på hvor hurtigt det er, men for det meste er det fint nok at arbejde med.


