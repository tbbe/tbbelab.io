---
layout: page
title: Bullet Journal
comments: false
---

Jeg har forsøgt at samle nogen nogen resourcer på denne side sådan at det bliver nemt at finde dem og komme igang med sin journalling. Jeg vil derudover gerne vise mine tips og tricks til hvad der virker for mig.
Begynder delen

Hvis du er nybegynder eller du slet ikke er begyndt endnu, så er det helt klart anbefalingsværdigt at se Ryder Carolls video om hvad en Bullet Journal er. Den tager ca 5 minutter, men tro mig de er givet godt ud, Se videoen her.

!(https://youtu.be/fm15cmYU0IM)

Videoen kommer fra denne adresse: [https://bulletjournal.com/pages/learn](https://bulletjournal.com/pages/learn) og der er helt klart andre gode ting der også, så husk at besøge den.
Jeg vil helt klart anbefale dig at starte med en minimalistisk indgang til Bullet Journalling. Start med det det som du kan se i videoen og det som står på [https://bulletjournal.com/pages/learn](https://bulletjournal.com/pages/learn) siden. Pinterest og Instagram er begge gode og næsten uudtømmelige kilder for inspiration til Bullet Journalling, men i starten kan det være noget bedre at holde sig til det simple og se hvad man har brug for inden man går all-in med trackers og andre collections som man i virkeligheden ikke har tid (eller lyst) til at vedligeholde. Der bliver massere af tid til at lave alle disse ting senere.
Når du så har fået styr på standard delen, ved at bruge dem i et par måneder, så kan du begynde at dyrke det der virker for dig i højere grad, og du kan droppe eller forbedre de ting som ikke virker for dig eller du ikke har brug for. Alt dette sker der mere af i næste sektion.

![Valg af notesbog](/assets/img/notesbog.jpg)

## Valg af notesbog
Der er en notesbog der slår dem alle sammen når det kommer til Bullet Journalling, men den er godt nok også dyr! Supernotesbogen er lavet af det tyske firma Leuchtturm 1917 og den koster ca kr. 200,-og kan fås i Bog og Ide samt Arnold Busck boghandler. Der er også en mængde online shops der har den.
MEN mindre kan bestemt også gøre det! - Hvis du gerne vil starte lidt mindre så ligger en linieret kinabog i den anden ende af spektret. Den koster vel omkring kr. 10,- hos Søstrene Grene eller i en Flying Tiger butik. Disse butikker har som regel også nogen notesbøger der er af almindelig god kvalitet som helt sikkert kan anvendes.

Du skal have fokus på størrelsen af notesbogen, for hvis det her kommer til at virke for dig, vil du formentligt have den med alle mulige steder, dels slider det på den, og dels skal den have en størrelse hvor det ikke er et problem at have den med. Jeg vil også anbefale dig at vælge en der kan ligge fladt på et skrivebord opslået. Det gør det en hel del lettere når man skal skrive i den.

## Rapid logging

Opskriften på rapid logging er at lave passende forkortelser opstillinger i de noter man tager så det går hurtigere at skrive men uden at man mister mening i forhold til hvad det er man gerne vil logge. Der er forskel mellem rapid logging og den korte form som den har og den mere almindelige måde at skrive i en dagbog hvor det mere er ens tanker som man skriver ned i klart sprog. Det er naturligvis plads i din Bullet Journal til at bruge den som dagbog på traditionel vis, men det er jo som regel ikke noget man gør som en del af et møde.
Hvis opgaven er “Lav interview delen af firma websitet færdigt og læg Lises interview online” vil det stå i min daglige log på denne vis:

* website: færdiggør interview del + Lises interview

Det kan jeg fint nå at skrive mens jeg deltager i et møde hvor vi snakker om hvad der skal laves ved websitet. Jeg kan også find på at starte opgaver med et navn og et kolon og så et emne. Det betyder at jeg skal snakke med den givne person om emnet. f.eks hvis jeg skal snakke med min kone om datoerne for vores næste ferie til New York:

* Bitten: Datoer for New York ferie

På måde kan man forkorte hvad man skriver og derved skrive det hurtigere.
Rapid logging handler også om at starte med det rigtige tegn. En dot betyder en opgave, her forventer man at der er opfølgning i form af at opgaven er løst på et tidspunkt. “-” betyder vide, altså noget som jeg ikke vil glemme. Det eneste der ikke passer ind her er passwords. Men der er ikke nogen opfølgning på viden, du kan bare slå det op senere hvis du har behov for det. Hvis du er i tvivl om det er noget du skal bruge senere, så noter det alligevel. Så har du muligheden for senere at slå det op. En ring “O” betyder en begivenhed, altså noget der sker på et tidspunkt som har betydning for dig. Dette kunne være et møde, det kunne også være at din bryllupsdag eller lign. Det betyder ikke så meget hvad begivenheden er, kun at den betyder noget for dig.
Hvis du får logget noget som ikke viser sig at være rigtigt eller er blevet irrelevant, så streg det over. Dette gælder opgaver som du skifter mening med og ikke vil udføre alligevel og det dækker også over information som viser sig at være forkert og begivenheden som ikke skete alligevel. Ret ofte logger jeg også grunden til hvorfor det er blevet irrelevant hvis den ikke er indlysende.

![Nested logging](/assets/img/bj1.png)

## Nested logging
Hvis du f.eks har information kytte til en begivenhed eller opgave, men hvor det ikke giver mening at skrive det som en del af navnet på opgaven eller begivenheden, så laver jeg et indryk lige neden under og laver en “-” for at angive jeg har noget information der knytter sig til det overordnede element.
Dette fungere også fantastisk til at logge mødenoter. Jeg laver en Begivenhed med mødets navn i starten af mødet. Hvis det er vigtigt starter jeg med at logge hvem der deltager. Efterhånden som der dukker nyttige informationer op på mødet, så loger jeg dem som nestede informations logs. Denne metode kan også bruges til at se om jeg egentligt burde have været til det møde. Hvis jeg ikke har logget nogen nyttige informationer under mødet, var det i store træk spild af tid eller jeg var for doven til at logge det. Hvis det var spild af tid, kan jeg jo bruge det som indikator for om jeg burde deltage en anden gang.

# Når du er igang

Nu er du så i gang, du har brugt standard Bullet Journal i en eller flere måneder. Du har sikkert fundet ud af at der er rigtigt meget inspiration at hente på både Pinterest og Instagram. Du har sikkert også fundet ud at der er virkeligt mange der gerne vil gøre deres sider så pæne som overhovedet muligt. Det er så her du skal passe på ikke at hoppe i “Neeej hvor er det flot” fælden. Det er jo meningen at det skal være funktionelt og gøre dit liv bedre, og det er der bare mange af de flotter ideer der ikke gør, alene fordi det tager for lang tid at tegne disse sider.
Prøv at holde dig til kun at tage nye ting ind når de eksisterende ikke virker. Det er svært at holde sig fra at prøve de nye skindende flotte ting, men hvis du ikke reelt har et behov er det næsten sikkert det ikke kommer til at spille for dig.
I forhold til nøgler har jeg fundet det nyttigt at udvide standard med et par ting. Det første er ngoet jeg har fra GTD verdenen, og det er at markere noget man venter på fra andre med en diamant, når så jeg ikke behøver at vente længere, så krydser jeg den ud. De steder hvor jeg bruger det her er f.eks hvis jeg sender en mail med en forespørgsel til nogen og det betyder noget for mig at de svarer, så notere jeg det med en diamant. Når så jeg gennemgår siden for review, så er det nemt at se om jeg skal følge op på det eller krydse den af fordi jeg har fået svar.

![Oversig over de nøgler jeg bruger i min Bullet Journal](/assets/img/bj2.png)