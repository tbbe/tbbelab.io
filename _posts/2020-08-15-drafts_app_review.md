---
layout: post
title: Drafts app review
tags: drafts_app app_review ipad iphone mac 
---  

![Dette er et blogpost skrevet i Drafts. Her er Markdown syntaks brugt til at lave de 3 overskrifter](/assets/img/drafts_app.png)


Denne app har til formål at være altid til stede på mine enheder for hurtigt at kunne fange de ideer jeg får samt at give mig et godt skriveværktøj med 100% markdown understøttelse. Dette er mit review af denne fantastiske app.

Jeg kan godt lide og skrive. Faktisk ville jeg en gang være forfatter. Når jeg er i skrive humør, skriver jeg forholdsvis hurtigt og jeg kender Markdown syntaksen ret godt hvorfor det næsten ikke tager noget tid at lave de par tegn der skal til for at formatere den tekst jeg skriver undervejs. Så det er mere eller mindre et must at mit skrive værktøj har fuld understøttelse af Markdown og Multi Markdown

Drafts er en 1-2-3 applikation i det den er inddelt i 3 områder:
1. Første område er at den kan bruges til at skrive dine ideen ned så snart du får dem. Du kan bare starte app’en, så er den klar til at tage imod ideen. Der er en widget der kan starte appen hurtigt, og hvis du vil automatisere at der kommer ny tekst i Drafts er der ret god support for Shortcuts autotatisering.
2. Andet område er det at skrive tesksten. Som jeg skrev oven over er der god support for Markdown. Hvis du heller bare vil lave almindelig tekst, kan du også det. Det korte og det lange er at Drafts kommer ikke i vejen for dig mens du skriver, og uanset hvor hurtigt du er på tasterne kan den følge med.
3. Sidste område er hvad du skal gøre med din tekst. Her er Drafts aldeles fantastisk. Den har nogen såkaldte actions som kan bruges til at få teksten videre. Drafts kommer med en del standard actions som kan få din tekst videre. Dette kunne dækker alt fra simpelt at kopiere teksten, sende den som en mail eller SMS/IMessage eller at uploade det som et nyt blogpost på din blog. Der er også omfattende Shortcuts support hvis man har lyst til at gå den vej.

Det med actions kan ikke lovprises nok. Der kommer en del med app’en som standard, og hvis der ikke er nok der, kan man hente en bunke mere fra app’ens hjemmeside. Det er for øvrigt integreret i appen at hente dem, så det er super nemt.

Drafts er app’en til tekst. Du behøver ikke rigtigt andet som jeg ser det 😀

# Pris

Drafts er gratis i sin basale form. Drafts har alle de funktioner du har behov for i den gratis udgave. Hvis du skal skal gemme dine tekst noter i Drafts kan du tilkøbe nogen organiserings værktøjer samt muligheden for at kunne lave dine egne actions (i JavaScript).
Jeg har ikke haft behov for at købe pro værktøjerne endnu, men jeg tænker lidt at det nok ender med at jeg gør det. Prisen for Pro værktøjerne er et abonnement på kr. 155,- om året.