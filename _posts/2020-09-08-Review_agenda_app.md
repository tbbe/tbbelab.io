---
layout: post
title: Review af Agenda app
tags: app_review agenda_app ipad iphone mac
---

![Agenda app](/assets/img/agenda_app.png)

> _Agenda app er en dato fokuseret notetagnings app. Datofokuseringen er ikke et krav, men det fedest funktionalitet fra denne app er centreret omkring det, så det er faktisk en stor ting. Dette er min mening om denne fanatiske app._

## Formål
Denne app er en notetagnings app, det er helt sikkert, men det er også helt sikkert den er mere end det. Agenda har en ret unik indgangsvinkel på det at organisere noter. Denne organisering er opdelt i kategorier. Kategorier indeholder sub kategorier eller projekter. Sub kategorier indeholder også projekter. Og til sidst: projekter indeholder noter. Disse noter er det det hele handler om. De enkelte noter kan være tilknyttet en dato eller en kalender begivenhed. Og noter kan indeholde links til en eller flere påmindelser fra Påmindelser app’en. Det lyder lidt rodet, men hvis man anvender Agenda på denne måde kan man sætte et komplet produktivitets system op, hvilket som jeg ser det, er dens formål. At holde styr på dit liv en note ad gangen.

## Funktionalitet

Agenda en en meget flot app. Alt er behageligt at kigge på. Den kan følge lys eller mørk tilstand alt efter hvad dit system er sat til og i det hele tager et det er super flot og intuitivt interface. Alt i agenda virker gennemført og veltilrettelagt. Dog kan udviklerne blive ved med at finde på features som de bygger ind i Agenda. Hvis man køber adgang til premium features, så får man adgang til disse nye features, hvor af de sidste to i skrivende stund er at have note templets samt det at kunne arkivere et projekt. Det skal lige siges at man behøver ikke betale for premium features for at bruge denne app, den er fuldt ud funktionel i den gratis version.

Som jeg beskrev under formål er der et hierarki af ting inden man når ned til noterne, og det er noterne det hele handler om. Resten er den nødvendigt organisering som er nødvendigt for at holde styr på noterne. Agenda tvinger dig til at have lidt styr på dine noter på den måde, hvilket jeg finder at være en god ting. Du har således ikke en note uden du minimum har et projekt. Og et projekt vil gå i en kategori. Men mit råd til dig er at starte med så lidt som du har brug for, du kan altid tilføje.

Fordi Agenda er tidsorienteret, kan man se hvad man laver på en tidslinje helt automatisk. Man kan godt have noter som ikke er bundet til et tidspunkt. Disse noter vil eksistere iblandt alle de noter man har lavet som er bundet til et tidspunkt, og det er er disse noters oprettelsestidspunkt der vil bestemme deres sortering i forhold til de andre noter. Dette syntes jeg fungere super fint.

Selve interfacet er delt op sådan at der er en midtersektion hvor ens noter er og hvor man laver selve redigeringsarbejdet. Så er der en højre og en venstre kolonne som begge kan skjules når man ikke har behov for at se på dem, hvilket er super smart på iPhone app’en. Højre kolonne indeholder note hierarkiet med kategorier, sub kategorier, projekter og søgninger. Søgninger kan gemmes hvilket er super nyttigt, men mere om det senere. Venstre kolonne indeholder data omkring datoer og kalender, påmindelser og noter som enten er relaterede eller som du har redigeret for nyligt. Dette er en super simpel opdeling af applikationen, det er nemt at finde alt og alt findes der hvor man forventer det.

Nu fik jeg sagt at søgninger kan gemmes. Og det skal lige uddybes lidt. Man kan nemlig søge på tags og personer også, så disse gemte søgninger bliver lidt mere som smart foldes. Hvis man nu holder øje med ting man skal tale med personer om, så kan man tagge disse noter med en person. Når så man har mulighed for at snakke med den person, så er det nemt lige at hive de noter frem som er tagget med personen bare ved at søge efter “@person”. Man kan lave det samme med tags, disse præfikses med “#” i stedet for @.

Man kan tagge noter ved at man hvor som helt i dem skriver et tag som er et ord der begynder med “#”, f.eks: #budget eller #avoid-at-all-cost. Hvis man vil tagge personer, så bruger man en “@” i stedet. Det er kun et ord stadig så hvis du vil have hele person navne skal du bruge bindestreger eller CamelCase, f.eks @ThomasBentzen eller @Thomas-Bentzen. Det er ikke smart at bruge underscores da Agenda også understøtter Markdown formatering hvor det at have underscores omkring et ord betyder kursiv, og bare for ikke at komme ud i misforståelser omkring det, så er det bedst ikke at bruge underscores til tags.

Og nu var det jeg nævnte Markdown. Du kan bruge markdown syntaks til at skrive dine noter med. Agenda konvertere det til sin egen syntaks, f.eks bliver “# Overskrift” til en H1 overskrift og “## Underorverskrift” bliver til en H2 overskrift. Det virker også med links og fed, kursiv og de andre formateringer. Det er også muligt at eksportere sin note i Markdown formatet. F.eks sker det ofte at det jeg skriver i Notion, som også understøtter markdown, starter sit liv i Agenda og så er det super at jeg ikke mister denne formatering når jeg vælger at flytte min note fra Agenda til en side i Notion.

Foruden tags og personer, kan man tilføje en mængde andet til sinde noter. F.eks:
* __Tegninger__. Hvis man bruger sin iPad og har en Pencil, så kan man tilføje tegninger til sine noter den vej.
* __Stjernemarkeringer__, man kan bruge disse lige som flag i et e-mail program, eller bare til at sætte fokus på en note. Stjernen sættes ud fra en sætning i noten så den kan være meget specifik
* Man kan scanne __dokumenter__ direkte til noten via samme funktionalitet som dokument scanneren i Noter app’en.
* Man kan indsætte __billeder__ enten fra kameraet eller fra foto biblioteket.
* Man kan __vedhæfte__ filer af enhver type, dog skal man være opmærksom på at applikationen bliver noget langsom indtil den har gemt vedhæftningen i iCloud. Det betyder at det er ikke super smart at gemme en 500mb videofil direkte i Agenda, men du kan også bruge links.
* __Links__ af enhver type. Disse opdages af sig selv hvis du paster dem ind fra andre apps.

# Hvordan jeg bruger Agenda

Inden jeg overhovedet begyndte at bruge Agenda var min holdning at jeg nok ikke rigtigt har brug for endnu en notetagnings applikation. Jeg har prøvet så mange det sidste stykke tid og de er alle samme fantastiske på hvert deres område, så jeg kunne ikke se der var plads til endnu en. Desuden prøver jeg at komme ud af nogen andre som er på abonnement basis. Jeg valgte imidlertid at prøve med Agenda alligevel nu jeg har hørt så meget godt om det. Og jeg er ikke blevet skuffet. Det er virkeligt et god app som levere på de områder hvor den skal.

Jeg bruger Agenda til at holde data som ændre sig meget, og som er relateret til mine projekter. Et arbejdslager om man vil. Jeg er GTD’er hvilket vil sige at alt hvad jeg skal enten er en meget simpel opgave nedskrevet i min OmniFocus eller det er et projekt. Projekter bliver sådan set også håndteret i OmniFocus, men OmniFocus er ret dårligt til notetagning. Jo man kan gøre det, den har et notesfelt og den kan håndtere virkeligt mange data, men det er ikke et super oplevelse. Dvs de projekter jeg har som kræver at jeg skal taget noter af dem, de bor i min Agenda. Det er så interlinked på den måde at OmniFocus projektet har et link til Agenda, og den første note i mit agenda projekt er en linned note med beskrivelsen af succes for dette projekt samt linket til OmniFocus projektet så det er nemt at skifte mellem de to. Det fungere fantastisk, og det tager næsten ingen tid at sætte op. Jeg tænker at når jeg bliver lidt bedre til at bruge Shortcuts, så kan jeg gøre det endnu bedre.

Derudover har jeg et par specielle projekter i Agenda også. Før jeg besluttede at gå over til iPad, brugte jeg en papir notesbog sammen med Bullet Journal metoden. Den del med rapid logging har jeg valgt at flytte til et projekt i min personlige kategori kaldet Rapid logning. Jeg laver så en ny note for hver dag hvor jeg samler de ting op som jeg ellers ville skrive i min Bullet Journal. Jeg skrifter projekt hver måned da de ellers bliver ret store. Jeg beholder sidste måneds projekt i kategorien, men jeg arkivere forrige måneds projekt. Så er der der stadig til at sige i hvis jeg har et behov, men det fylder ikke op i min oversigt. Jeg overvejer at flytte dette til sin egen kategori, men har ikke gjort det endnu.

Jeg kører også familiens madplanlægning i et projekt under Familie og hjemmet kategorien. Det giver mening det er mig der gør det, for det er også for det meste mig der laver maden. Vi bruger at bestille mad for en 4-6 dage af gangen og så hente disse, og det er her jeg planlægger hvad vi skal købe. Jeg har så disse noter stående som u-daterede noter som jeg så giver en dato når jeg ved hvilken da vi laver hvilken ret. Dette er super fleksibelt og lige nok form til at give den funktionalitet jeg har behov for. Selve madopskrifterne bliver linket ind fra Notion eller DevonTHINK To Go.

Mit råd til dig der skal igang med Agenda er at du skal starte småt. Det er super nemt at slå sit system stort op og man når meget hurtigt en størrelse hvor det bliver en sur opgave at vedligeholde. Start med en kategori med de projekter du har behov for, ikke mere, og byg langsom ud fra det. Du skal ikke være bange for at slette og gå tilbage hvis du kommer ud af en tangent. Held og lykke med det.

# Pris

Agenda bruger en ikke før set freemium model som efter min mening er intet mindre end genial. Selv applikationen er gratis, og den findes til alle Apple platforme på nær Apple TV. Derudover opfinder udviklerne hele tiden nye funktioner til Agenda. Man kan købe et års opdateringer til premium features, enten på iOS og IPadOS eller til Mac også. De features der så bliver opfundet det næste år får du med sammen med alle dem som er opfundet op til nu. Pris for iPadOS og iOS for et år: 129,- kr Pris for iPadOS og iOS samt Mac for et år 259,- kr.

# Referencer

Agenda har et meget engageret fælleskab som du kan melde dig ind i på [ttps://agenda.community](https://agenda.community). Agenda har en hjemmeside som indeholder mange guider og gode forklaringer til brug af app’en: [https://agenda.com](https://agenda.com)

Der er ikke mange videoer at finde på Youtube som er noget værd hvis du spørg mig, men det kan være det ændre sig efter hånden som Agenda bliver mere udbredt.

Der er en del podcasts omring Agenda. Her er et par stykker af dem som jeg har hørt, og som jeg mener kan hjælpe på en eller anden måde:
* Nested Folders ep. 30 – [Scottys Agenda Setup](https://nestedfolderspodcast.com/podcast/episode-30-scottys-agenda-setup/)
* Nested Folders ep. 26 – [Rose’s Agenda Setup](https://nestedfolderspodcast.com/podcast/episode-26-rose-agenda-setup/)
* iPad Pros ep 56 – [Agenda and Project Management with Pat Maddox](https://ipadpros.net/2019/10/24/episode-65-agenda-and-project-management-with-pat-maddox/)