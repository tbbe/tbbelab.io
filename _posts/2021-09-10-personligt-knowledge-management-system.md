---
layout: post
title: Personligt knowledge management system (PKMS)
excerpt_separator: <!--more-->
---
# Personligt knowledge management system (PKMS)

Publiserings dato: November 19, 2021
Tags: bulletjournal, noter, viden

![PKMS screenshot](/assets/img/pkms_screenshot.png)

> En af de ting der er en del fokus på alle mulige steder fra i øjeblikket er at have et personligt videns system. 
> Og der er ingen tvivl om at det er en super god ide, men hvordan får man lavet et sådan et på en smart måde? - 
> jeg har kigget lidt på emnet, og jeg tænkte at jeg ville dele hvad jeg har fundet ud af, også selv om jeg 
> udemærket ved at du i din Process med at lave dit eget system, kommer til at gøre det meste af hvad jeg har gjort igen selv.

<!--more-->

# Hvad er et personligt videns system (PKMS)?

Det er nok smart lige at definere det lidt inden vi går i gang, og den bedste måde jeg kan beskrive det på er følgende.

## Måske du har fundet ud af det er en god ide at tage noter?

Jeg personligt kan ikke huske en skid! Jeg ved ikke hvor ofte jeg har været i den situation at jeg har fået noget at vide som på det tidspunkt var super vigtigt, så jeg behøver helt sikkert ikke skrive det ned, for jeg skal bruge den viden om et par minutter, og så hurtigt glemmer jeg heller ikke… for at finde ud af at jeg er nødt til at gå tilbage og spørge igen efter bare 5 minutter. 

Halveringstiden (for at bruge et udtryk fra fysikkens verden) for mig for viden som jeg ved jeg skal bruge er vel omkring 2 minutter. Så efter 5-6 minutter kan jeg huske så lidt at jeg ikke kan stole på det. For mig er det en no-brainer (ha ha) at jeg er nødt til at skrive ting ned.

## Måske du har fundet ud af at de noter du tager har en vis værdi, og nogen af dem er værd at gemme?

Det har jeg i hvert fald fundet ud af. Men det kommer med en ny række problemer. Hvor skal de gemmes henne, og hvis jeg gemmer dem, så skal jeg jo også gerne kunne finde dem igen, hvad er den mest rigtige måde for mig af de mange muligheder der er. Der er så mange muligheder er og mange af dem er gratis, nogen koster lidt og andre koster meget. Jeg ved at jeg er super nærig omkring det her, så prisen vejer ret tungt for mig.

## Måske du har fundet ud af at det betyder noget hvordan du tager noter?

En notesbog eller et stykke papir er super godt startpunkt for at tage noter. Men man skal ikke bruge det ret lang tid før det kan blive et problem at holde styr på. BulletJournal har defineret et indeks som tager sig af denne ting, men det er ret svært at tilføje en YouTybe video til en notesbog, så du har stadig begrænsninger her. 

Så begynder man at kigge på de digitale værktøjer, og så bliver mulighederne ret hurtigt meget store, og det er virkeligt svært at vælge...

## Måske du savner en struktur for opbevaringen af dine noter så du kan finde dem igen?

En note med lige netop den information på som du skal bruge, har en meget lille værdi hvis du ikke kan finde den på det tidspunkt hvor du skal bruge den. Men hvis du kan finde den er den, er den næsten uvurderlig. 

Digitale værktøjer gør det ment at søge efter ting, men her er man så underlagt kvaliteten af den søgemaskine man bruger. Ellers kan man lave taksonomier og andre sammenkædninger for at finde sin information. I den analoge verden er man 100% afhængig af indekser samt taksonomier, men hvad er den smarte måde at lave dette på?

## Hvis du kan besvare alt det oven over konkret og præcist, så har du et PKM System!

Videns → Opsamling

Videns → Opbevaring

Videns → Sammenkædning

Videns → Review og forbedring

Videns → Brug

# Noter

Jeg lærte at tage noter i folkeskolen, og selv om alt det jeg lærte var passende for tiden, så er der meget lidt af det jeg lærte dengang, som jeg kan bruge til noget i dag. Noter er så meget mere, og efter der er kommet gode digitale værktøjer til som virkeligt hjælper med at holde styr på ens noter så vel som at tage dem i det første sted, så ser verden meget anderledes ud.

Derudover bliver der også skælnet mellem to situationer i forhold til det at tage noter. Vi har ikke gode ord for dette på dansk, men de engelske er "note taking" og "note making". Begge dele kalder vi at tage noter på dansk, hvilket sådan set også er fint nok. Det der menes med de engelske navne, så vidt jeg har forstået, er dette

### Note taking

Når man tager noter så gør man det i gammeldags forstand. Man tager en noter af det man læser / hører / ser for at kunne huske det for fremtiden af. Helt klassisk.

### Note making

Handler mere om at man tager en hurtig note af en ide man har fået, sædvanligvis for at kunne huske den, og så arbejder man senere videre med den note sådan at den bliver bedre over tid, og måske bliver linket til andre noter / ideer. Denne type af noter er derfor meget mere levende. 

## Note tagning

Der er derudover mange forskellige måder man kan tage noter på. Jeg har her samlet en lille liste over de metoder jeg er stødt på i den research jeg selv har lavet omkring emnet.

### Zettlekasten

Denne metode er der super meget snak om på nettet i øjeblikket, og det har der sådan set også været di sidste par år. Metoden er opfundet af Niklas Luhmann, og han har brugt systemet til at udgive mere en 70 publikationer, det er en ret så produktiv herre, og efter hans eget udsagn er det gjort muligt ud fra hans Zettlekasten notetagnings system. Se mere på hans wikipedia side her:

![Niklas Luhmann](/assets/img/250px-Luhmann.png)

[Niklas Luhmann - Wikipedia, den frie encyklopædi](https://da.wikipedia.org/wiki/Niklas_Luhmann)

Dette system var 100% analogt, baseret på indeks kort i kasser. Noterne var baseret på et indeks system, og de var også kædet sammen via dette indeks system. Det smart fundet på og når man nu ikke har computere til at hjælpe sig med at indeksere sin viden og hjælpe med at kunne søge i det, så er det en fantastisk måde at gøre det på. Jeg vil helt klart ikke anbefale at du forsøger at replicere dette system til eget brug, da den digitale alder vi lever i lige nu giver nogen andre muligheder som bestemt vil være en hjælp hvis man skal i gang med at få styr på sine noter. 

Den eneste situation jeg vil anbefale dette system er hvis du gerne vil have et eget system som er 100% analogt og du gerne vil holde det sådan for nu og mange år frem, så er det her klart en god måde at gøre det på. Men hvis du bruger bare lidt fra den digitale verden, bør du se på Smart notes eller Evergreen notes i stedet.

### Smart notes

Konceptet smart notes er opfundet af Sönke Ahrens, i hans bog kaldet How to Take Smart Notes. Bogen kan fås her:

[Få How to Take Smart Notes af Sonke Ahrens som Paperback bog på engelsk](https://www.saxo.com/dk/how-to-take-smart-notes_sonke-ahrens_paperback_9781542866507)

Sönke er professor, og hans indgangsvinkel er at lære hans studerende at holde styr på deres viden. Jeg har hørt meget godt om denne bog selv om jeg ikke har taget mig sammen til at læse den endnu, men jeg ved med sikkerhed at jeg gør det. Bogen indeholder alt hvad du skal bruge for at sætte et super funktionelt note tagning system op.

### Evergreen notes

Dette er Andy Matuschaks fortolkning af Smart Notes og Zettlecasten. Han har gjort en mængde af hans egne noter om emnet tilgængelig online her:

[Evergreen notes](https://notes.andymatuschak.org/Evergreen_notes)

Hvis du går efter en digital implementation, så vil jeg anbefale at læse hans noter igennem, da de afspejler nogen erfaring med systemet foruden hvordan dette er bygget op, derudover en del regler / forholdsregler at tage for at holde dit system godt opdateret og overskueligt.

### Progressive summarization

Tiago Forte lavede i sin tid et cohort baseret kursus kaldet Building a Second brain. Her var det værktøj der var valgt og det som alt materiale var baseret på Evernote. Jeg var med i den første udgave af det kursus, og det er nok første gang jeg har fået noteværktøjer jeg kunne bruge til noget. Specielt denne progressive summatisering som i alt sin enkelthed går ud på at hver gang du gennemlæser en note, så laver du en summetisering af den, dels ved at bruge et highlighter værktøj, og dels ved at sætte ord og sætninger i fed og kursiv.

Siden er kurset blevet udvidet betydeligt. Det køres stadig regelmæssigt hvis du vil have en plads på det. Se mere her:

[Building a Second Brain: Capture, Organize, and Retrieve Your Ideas Using Digital Notes](https://www.buildingasecondbrain.com/)

### Bullet Journal

En bullet journal er sådan set hele dit liv i en notesbog. Den er et all round værktøj til at hjælpe med stort set alt i dit liv. 

Bullet Journal er opfundet af Ryder Caroll som selv lider af ADD, og han skulle bruge et værktøj som kunne hjælpe ham med at overkomme de ting som bliver svært når man har ADD. Det viser sig så at han i den forbindelse har opfundet et værktøj som generelt er super anvendeligt. Man kan bruge en Bullet Journal til som videns opsamlingsværktøj, dog vil jeg anbefale at man ikke bruger det til videns opbevaring, med mindre man opfinder et hybrid system da det ellers kan være noget besværligt at finde sine noter frem igen når man skal bruge dem.

# Værktøjer

Der er naturligvis en lang række værktøjer som man kan bruge til at hjælpe med noterne. Både selve det at tage en note, og det at opbevare den og finde den frem igen når man skal bruge den. 

## Digitale værktøjer

I denne sektion vil jeg gennemgå de værktøjer jeg kender og har en mening om. Dette er på ingen måde en udtømmende liste, men det er de værktøjer som jeg har noget at bidrage med til.

### Markdown

Markdown er som sådan ikke et værktøj, det er et format. Det er oprindeligt opfundet af John Gruber fra [daringfireball.com](http://daringfireball.com). Han opfandt det for at bruge det som blogging format da han hellere ville skrive tekstfiler end rode med CMS systemer. Det er så noget resten af verden har taget til sig og videreudviklet til noget der nu effektivt kan erstatte tekstbehandling også. En mængde af de værktøjer vi skal se på understøtter dette format, og det ser ud til at det bliver mere og mere udbredt, så der er god grund til at blive gode venner med det.

Her er et markdown cheatsheet da det meget præcist forklarer formatet. Nu er det et cheetsheet, men det er sådan set næsten af betragte som komplet dokumentation:

[Markdown Cheat Sheet | Markdown Guide](https://www.markdownguide.org/cheat-sheet/)

### Obsidian

Obsidian er et meget nyt værktøj i det at det i skrivende stund kun er et par år gammelt. Værktøjet er lavet som det der hedder en Elektron applikation, hvilket betyder at det sådan set er en web applikation pakket ind i en dedikeret Chrome browser. Det betyder at det ikke ligner de andre værktøjer som du har på din desktop. og det er det samme værktøj uanset om du er på Windows, Linux eller Mac. Jeg er selv Mac bruger, og det ligner ikke nogen af mine andre applikationer, hvilket er et minus hvis man godt kan lide Mac OS polerede overflade. Og netop æstetikken er årsagen til at jeg valgte dette værktøj fra. Dog skal det siges at der er mange der virkeligt elsker dette værktøj og bruger store mængder af tid i det dagligt. 

En af de dedikerede fans er [David Sparks](https://www.macsparky.com) som er en advokat fra Californien. Det er meget usædvanligt for advokater at bruge andre værktøjer en Windows med MS Office og et journaliseringssystem til advokater. David bruger Macs, Obsidian og DEVONThink og er meget nytænkende indenfor hans felt. Tjek hans hjemmeside ud for flere tips og tricks.

I forhold til at vide mere om Obsidian som værktøj vil jeg fremhæve Linking your Thinking som ledes af Nick Milo. Nick har nogen fantastiske ideer om hvordan man kan opbygge et videns bibliotek baseret på noter. Der er en del videoer på Youtube fra ham omkring emnet, og jeg vil klart anbefale at se lidt nærmere på sagerne:

[Linking Your Thinking](https://www.linkingyourthinking.com/)

Hvis du ønsker at se mere på Obsidian så kan du finde det her:

[Obsidian: A knowledge base that works on local Markdown files.](https://obsidian.md/)

### DEVONThink

DEVONThink er et Mac, iPad og iPhone værktøj til at håndtere alt viden, dog mest et Mac værktøj. Jeg bruger dette værktøj som et meget avanceret arkivskab, men værktøjet har mange flere features end dem jeg bruger. Årsagen til at jeg ikke bruger dette værktøj til alt min videns opsamling er at det kun findes i Apple verdenen, og oftest tvinger kunder mig til at arbejde på Windows. Jeg er derfor nødt til at bruge et værktøj der er cross platform.

For dette værktøj vil jeg pege på to personer til at hjælpe dig videre, den første er [Kourosh Dini](https://www.kouroshdini.com/). Han bruger DEVONThink dagligt til at styre alle hans noter hvilket han har stor succes med. Bl.a. understøtter DEVONThink Markdown dokumenter som Kourosh har baseret sit videns bibliotek på. Kourosh har bl.a. også skrevet bogen Taking Smart Notes with DEVONThink som kan købes på [hans hjemmeside](https://www.kouroshdini.com/course-books/). Den anden person jeg vil pege dig i retning af er David Sparks som har lavet et kursus på DEVONThink. Jeg har taget dette kursus og fået stor gavn af det, og hvis du har planer om at burge DEVONThink, så vil jeg klart anbefale [hans kursus](https://learn.macsparky.com/p/dt). Du finder mere infor om profuktet her:

[DEVONtechnologies | DEVONthink](https://www.devontechnologies.com/apps/devonthink)

### Notion

Notion er en af de mest alsidige videns værktøjer der findes. Det fungere ved at kombinere en traditionel relationel database med en dokument baseret database. Det er således muligt at opbevare dokumenter både i form af en database tabel og en notes side, og her kommer det fantastiske, eller begge dele samtidig. Det skal forstås sådan at et entry i en database tabel også er en notes side som kan indeholde en mængde elementer til at opbevare viden som f.eks tekst, billeder, dokumenter og andre database tabeller, enten i form af linkede eller indlejrede tabeller. Dette til sammen gør at man har ret frie hænder til at strukturere sin viden som man gerne vil. Dog er der ingen mulighed for at gennemtvinge struktur da alt er freeform og kan ændres hele tiden. Det bevirker at værktøjet er mindre anvendeligt i en team sammenhæng hvor nogen af teamets medlemmer ikke har en disciplineret holdning til værktøjet. Men som personligt videns opbevaring og opsamlings værktøj er det intet mindre end fantastisk.

En af de mest vidende personer i forhold til Notion er Canadieren [Marie Poulin](https://mariepoulin.com/). Marie har sammen med sin mand og nogen andre Notion eksperter kurset [Notion Mastery](https://notionmastery.com/), som jeg er i gang med i skrivende stund. Marie er også meget aktiv på [Youtube](https://www.youtube.com/c/MariePoulin), og hvis Notion er dit værktøjsvalg så vil jeg klart anbefale at se hendes videoer.

Notion er online only, så man kan ikke tage sine data med på en flyrejse med mindre man betaler for inflight WIFI. Det er imidlertid ikke en showstopper for mig da jeg stort set altid er online, og i den tid jeg har brugt Notion kun har set det være nede i en times tid en enkelt gang.

Se mere om Notion her:

[Notion - The all-in-one workspace for your notes, tasks, wikis, and databases.](https://www.notion.so/)

PS: det er også Notion der driver denne webside 😉

### TheBrain

TheBrain er et værktøj jeg har brugt on og off i mere end 20 år. TheBrain er et super brugbart værktøj som er virker ved at have en Mac / Windows baseret desktop applikation som kan alt man nogen sinde har behov for i forhold til videns strukturering. Der er så også app til iPad, iPhone og Android som virker til at kunne tilgå sine data, dog ret begrænset og det er ogås en web applikation man kan anvende. Denne er dog endnu mere begrænset. 

Der bliver hele tiden arbejdet på at udvikle funktionaliteten og for et par år siden bliv hele applikationen skrevet om fra en Java GUI applikation til et mere moderne framework. 

Selve notes sektionen i værktøjet er baseret på Markdown og notes organisationen er baseret på et visuelt system der virker lidt ligesom ens hjerne, kaldet the plex. TheBrain skal opleves, ikke forklares, og det har de heldigvis lavet en video til:

[https://www.youtube.com/watch?v=QEfvCE-vKTI](https://www.youtube.com/watch?v=QEfvCE-vKTI)

Hvis du er til visuel videns opbevaring, og du kan have denne applikation installeret på dit arbejde, så vil jeg klart anbefale at se nærmere på TheBrain:

[TheBrain: The Ultimate Digital Memory](https://thebrain.com/)

## Analoge værktøjer

Det kan jo også du gerne vil bruge rigtigt papir og dit favorit skrive redskab. Det skal jo ikke afholde dig fra at kunne lave et godt videns opsamling og opbevarings værktøj.

### Bullet Journal

Det system jeg personligt har haft mest succes med er Bullet Journal. Jeg har lagt det på hylden mens jeg sætter mit Notion baserede system op, men der har været mange situationer hvor jeg har haft stor lyst til at gå tilbage til min Bullet Journal. 

Hvis man følger standard opsætningen af Bullet Journal som Ryder forslår i hans video, så har man faktisk en super fantastisk værktøj til alt videns opsamling, og hvis man gemmer de ting man gerne vil opsamle i collections som kommer i ens indeks, kombineret med at man bruger threading til at kæde sine collections sammen på tværs af notesbøger, så har man også et super godt værktøj til videns opbevaring. Jeg vil anbefale at starte med at se Ryders video om hvad Bullet Journal er, og hvis det fænger, så har han skrevet bog som er super fantastisk kaldet The Bullet Journal Method.

Video:

[https://www.youtube.com/watch?v=fm15cmYU0IM](https://www.youtube.com/watch?v=fm15cmYU0IM)

Og bogen:

[The Bullet Journal Method](https://bulletjournal.com/pages/book)

### Arkiv system

En af de mest anvendelige former for arkiv systemer jeg nogen siden har brugt er et fuldkommen standard arkivsystem baseret på A til Å fordelt ovre så mange arkiv skuffer du har adgang til. Nu er det jo ikke altid man selv har kontrol over hvilke kontormøbler der bliver indkøbt, men det er helt klart min anbefaling ikke at gå med Esselte arkiv skabe som er baseret på side åbnede skabe med deres hænge arkiv system. Det bedste man kan få efter min mening er et standard arkivskab med skuffer hvori man hal mapper som står op og bliver holdt sammen af en fleksibel bagstop. 

Husk at navngive dine mapper tydeligt, da det gør det meget nemmere i forhold til at skulle finde informationen når det er man skal bruge det.