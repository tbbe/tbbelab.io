---
layout: post
title: En lille snak kom den daglige log i Bullet Journal
tags: bullet_journal noter notetagning
--- 

# En lille snak om den daglige log i Bullet Journal

> _Hvis man kigger sig om på Pinterest, Instagram og Facebook, så vrimler det med diverse mere eller mindre kunstneriske indspark på hvordan en daglig log ser ud. Det er næsten altid “kun” en samling todo, og det er synd, for de daglige logs kan så meget mere end det._

Denne er artikel har primært til hensigt at give et, på ingen måde udtømmende, bud på hvad man også kan bruge de daglige logs til.

Hvis man ser Ryders materiale, så advokere han for yderligere 2 informationstyper i den daglige log, nemlig Event identificeret med en cirkel og information som er identificeret med en streg. Lad os starte med events.

## Events

Events er identificeret med en cirkel og kan udfyldes forud i tiden eller efter de er sket, eller i et miks. Jeg burger selv den sidste tilgang i det at hvis der sker noget i løbet af dagen som jeg ved har et tidspunkt, så lægger jeg det ofte ind med et event som har et tidspunkt. Hvis det så ikke sker alligevel streger jeg det bare over.

Det kan også være events som sker som der er værd at logge, altså events som bare skete. Disse kan være lige så værdifulde at logge som dem der har et tidspunkt tilknyttet.

Hvorfor skal man så logge events? - jeg gør det fordi jeg gerne vil vide hvornår ting er sket, specielt hvis det er noget der er vigtigt. Mange studier har vist at vores hjerner klarer denne del ret så dårligt. Vi kan simpelt hen ikke huske hvilken dag noget er sket med mindre vi har et ankerpunkt i hukommelsen. Det er så meget nemmere når man har skrevet det ned i en notesbog, og hvis det viser sig at man aldrig skal bruge det igen, så kan man bare strege det over.

## Information

Den anden del Ryder snakker om er information man ikke vil glemme. Dette identificeres som en streg. Jeg logger mange ideer på denne måde. Ideer som jeg ellers ville have ladet går tabt. Men det er bestemt ikke det eneste jeg logger. Jeg vil alt hvad der liger noget jeg muligvis kunne bruge igen i fremtiden. Jeg har bevist overfor mig selv mange gange at jeg ikke kan stole på min hukommelse overhovedet. Hvis jeg med sikkerhed ikke vil glemme det, så skal det skrives ned i min notesbog.

Eksempler på hvad jeg logger er alt vigtigt information jeg får til et møde f.eks. Jeg logger først sleve møder som en event, og så logger jeg informationen indlejret. Hvis jeg har møder hvor jeg ikke skriver noget ned, så er det et kraftigt vink med en vognstang om at jeg nok ikke skulle have været med til det møde.

Jeg kan også logge indformation indlejret under en opgave. Dette er for at specificere detaljer om opgaven, det er også en super smart ting at gøre.

Hvis jeg logger en ide eller et citat som jeg gerne vil gøre tidligt når jeg ser siden igen, så giver jeg det et udråbstegn eller en stjerne. Stjerne siger at det er vigtigt, og udråbstegn siger at der er noget inspiration at hente her.

Ryder siger også at han nogen gange har behov for at skrive long-form tekst i sin bullet journal. Dette er for terapeutisk at komme af med noget eller noget i den retning. Lige siden jeg har læst Ryders bog, hvor jeg fandt ud af det her, har jeg ment det er super smart, men jeg tror kun jeg har gjort det 4-5 gang over det sidste halve år. Så det er helt klart ikke noget jeg går tit, men når jeg gør det er det også rart at gøre.