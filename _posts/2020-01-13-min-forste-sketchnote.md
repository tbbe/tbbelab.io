---
layout: post
title: Min første rigtige sketchnote 
tags: sketchnote illustration bullet_journal
---
 
Ok - det her er lidt underligt at skrive om på denne måde, men jeg har lyst til det alene for at fortælle om det her scketchnote hejs.
 
Jeg har lige lavet min første sketchnote. Jeg har taget tilløb til dette i lang tid, for jeg har altid været af den overbevisning at jeg ikke kan finde ud af at tegne. Nu har jeg så overvundet det og nu ved jeg med sikkerhed at jeg ikke kan finde ud af at tegne, som i absolut helt sikkert, men det jeg også har fundet ud af er at det behøver man heller ikke :-) - se selv:

![Sketchnote](/img/img/sketchnote.JPG)

Ja det er tegnet med blyant, og billedet er taget med min telefon i dårligt lys, så hvis du virkeligt vil se detaljerne er du nok nødt til at klikke på billedet for at se det i stor størrelse. Det er Sarah Knights TED talk om not giving a fuck som jeg har forsøgt at sketchnote. [Se den her](https://youtu.be/GwRzjFQa_Og).

Og hvorfor så sketchnote i det første sted? - jo ser du, det at tage noter ved vi hjælper med at huske ting. Det giver også en dybere forståelse af det emne du tager noter af. Ved at bruge sketchnotes bliver det sjovere at tage noter. Derudover for du genereret nogen noter som virker mere inspirerende, og som du gerne vil se igen. Det bidrager til at du husker det endnu bedre og at du får en endnu dybere forståelse af emnet. Det er helt klart værd at bruge den tid det tager at lave en sketchnote, og til reference så er Sarahs TED talk ca. 13 minutter lang, og det har vel taget mig 25 minutter at lave denne sketchnote. Worth it!