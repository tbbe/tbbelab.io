---
layout: post
title: Hvad er den bedste model for betaling for software
tags: app_store betaling_for_apps
---

Der har været en del forskellige modeller her på det sidste, og rigtigt mange software udbyder lige fra den enkelte udvikler til det store etablerede softwarehus har i den grad taget SaaS og abonnements modellen til sig. Jeg er på ingen måde fan af den model og jeg vil vælge applikationer fra hvis jeg ikke kan købe en permanent licens til en version. Jeg har været nødt til at give slip på IMindMap da den blev til Ayoa.com ud fra den betragtning. Men inden man ser sig om så er det ret meget guld der smutter ned i abonnements kassen hver måned, og det gider jeg ikke.

En anden vi har set er freemium modellen. Den er også meget udbredt og har som jeg ser det to anvendelser:
1. For applikationer som måske ikke er gode nok til at nogen vil betale for dem up front. Når de så installere appen fordi den er gratis kan det være at kunder gerne vil betale for “pro features” eller vil gerner give et tip til udvikleren. Der er mange indie udviklere der bruger denne til perifære applikationer.
2. Man vil gerne give en basic applikation til mange brugere gratis. Dem der så bruger applikationen meget kan tilkøbe ekstra features som abonnement. Drafts er et godt eksempel på denne model.

Men så kom der en ny version af denne, og jeg vil tro det er den bedste model der findes lige nu:

Agenda er en notetagnings app som er dato orienteret.

Det super seje er deres freemium model. Man kan købe et abonnement til at få de pro features de tilføjer de næste 12 måneder for en pris. Det er ikke et abonnement, du skal købe igen om et år hvis du fortsat vil have flere pro features.

Det betyder at udviklerne committer til hele tiden at lave nye pro features som er værd at betale for, hvilket er godt for både burgeren og udvikleren.

Udvikleren tvinger sig selv til at holde sig i gang og komme med en lind strøm af forbedringer, forbrugeren har mulighed for at vurdere om det giver mening at betale for næste års pro features.

Det skal lige tilføjes at applikationen bestemt er super brugbar i sin gratis version, og pro features er lige netop det, pro. Hvis du bare har behov for den basale brug af applikationen, så er den gratis version fuldt ud dækkende. Men hvis du virkeligt bruger den avancerede del, så giver pro features virkeligt mening, hvilket betyder at jeg som burger virkligt føler jeg få noget for pengene når jeg køber hele pakken. Kunne dette være den perfekte fremium model? - jeg tror det.

Så er der jo den gamle og velafprøvede perpetuel licens til en version af en applikation. Det er min favorit. Her ved jeg hvad jeg køber. Jeg kan tjekke inden om jeg vil betale prisen for denne licens, giver det mening for mig, eller holder cost/bennefit. Det er noget mere ugennemskueligt med en abonnements model.

Hvordan fungere det bedst for dig?