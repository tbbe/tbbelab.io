---
layout: post
title: iPad som notesbog
tags: ipad noter notetagning
---

![iPad som notesbog](/assets/img/ipad_notesbog.png)

>_I denne artikel beskriver jeg hvad du skal bruge for at anvende iPad som en notesbog både i hardware og software_.

## Hardware
Du skal bruge en iPad der har Pencil support, da de stylus man ellers kan få til iPad ikke er penge værd, heller ikke dem til 5 kr i Flying Tiger. Heldigvis er de fleste nye iPads alle sammen med Pencil support. Derudover skal du også have enten en Apple Pencil eller en Logitech Crayon. Apple Pencil er bedst, sådan spec mæssigt, men hvis du har en Crayon, så prøv om ikke den er fin nok inden du bruger mange penge på en Apple Pencil. Det er også en god ide at du har et cover til din iPad som kan ligge fladt sådan du kan skrive og tegne uden at skulle tage iPaden ud af coveret hele tiden.

Jeg bruger slet ikke et cover, jeg har enten min iPad siddende på mit Magic Keyboard eller liggende på bordet uden cover.

# Apps

## Apple Notes
Din iPad kommer en et notes app som kan bruges til at tage noter med Apple Pencil i. Denne app giver mulighed for at blande dine håndskrevne noter og tegninger med billeder og andet grafisk indhold så vel som almindelig tekst. Alt dette indhold lægges i blokke som ikke kan overlappe hinanden hvorfor det virker noget irriterende. Apple Notes kommer også med en mulighed for at kunne konvertere håndskrevet tekst til almindelig tekst, kalender detaljer eller hvis man skriver et telefonnummer eller en e-mail adresse ned håndskrevet kan man ringe til nummeret eller sende en mail til e-mail adressen. Sjove gimmicks som ikke er ret anvendelige i dagligdagen. Det bedste man kan sige om Apple Notes er at den er gratis 🙂

## Good Notes
En god app til at lave tegninger og taget håndskrevne noter er Good Notes. Den koster lidt, ca 70,- kr. Men så dækker den også iPhone, iPad og Mac. Det er efter min mening den notes app med de bedste note organiseringsmuligheder, og der er nemt at rette udseendet af sine notesbøger til. Denne app har desuden ret god figur genkendelse og kan fint tage noter på PDF dokumenter. Man går ikke helt galt i byen med denne app.

## Notability
Denne app bruges ret ofte af folk der går til mange møder eller studere da den har en super godt integreret optagefunktion. Denne app koster ca 80,- kr og så er iPhone, iPad og Mac dækket ind. I forhold til note organisering er det kun 2 niveauer at opganisere i, hvilket kan være for lidt for nogen. App’en har rimelig god figur genkendelse men man er lidt begrænset i forhold til hvad man kan gøre med de figurer når man har lavet dem. Denne app er god til at tage noter på PDF dokumenter og kan importere en lang række dokumenttyper som kan konverteres til PDF. Dette er også en ret god app.

For se meget tilbundsgående reviews af notetagnings apps til iPad vil jeg henvise til [PaperlessX kanalen kanalen på YouTube](https://www.youtube.com/c/PaperlessStudent).

Jeg personligt burger ikke min iPad til at tage noter på på den måde. Det var planen da jeg købte denne iPad at jeg ville gå helt papirløst og kun bruge min iPad til alt skrive arbejde. Men jeg havde kun haft den i et par måneder før end jeg gik tilbage til min gode og velafprøvede Bullet Journal papirnotesbog. Det jeg anvender min iPad og min Apple Pencil til er tegninger, diagrammer og mindmaps. Men det er et separat blogpost.