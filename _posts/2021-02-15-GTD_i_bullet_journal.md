---
layout: post
title: Hvordan man bruger GTD metodikken i Bullet Journal
tags: GTD bullet_journal
---

Dette er en artikel om hvordan man kan flette sit GTD system ind i sin Bullet Journal uden at gå på kompromis med nogen at de gode ting ved begge dele. Dette er ikke den “rigtige” måde at gøre det på, der er en af mange rigtige måder, og det er i hvert fald det jeg endte med.
For at læse denne artikel er det en god ide at kende GTD metodikken, men i det mindste skal du kende disse to koncepter:
* __Next action__. - den næste fysiske ting du skal gøre for at bringe noget tættere på at være færdig. Dette ligner en todo fra en todo liste, men der er en betydelig forskel: En next action beskriver start punktet for at fortsætte, hvor en todo beskriver slut resultatet. F.eks så vil en next action kunne være: Begynd på udkast til artikel om oddere i Danmark, hvor todo’en til samme opgave ville være: Skriv artikel om oddere i Danmark. Det betyder rigtigt meget når man er i kampens hede at man ikke committer til at gøre hele artiklen færdig, men i stedet bare skal starte på den. Man vil formentlig gøre den færdig i begge tilfælde, men det er nemmere at starte på opgaven når man ikke lover sig selv at gøre den færdig.
* __Projekt__. - et projekt er noget som kræver mere end en next action for at blive færdig. Lige så snart man skal bruge mere end et trin, så er det smart at have en eller anden form for påmindelse om at man har gang i et projekt. Dette skal naturligvis på projektlisten som er en vigtig del af den ugentlige gennemgang, men det vigtige her er at hvis der er mener end to, så er det et projekt.

# Setup

Kontekst er den første diskriminator som bruges til at bestemme hvilke next actions som du kan udføre netop nu. Derfor er det smart at have sine next actions delt ud på __kontekst lister__ baseret på de lokationer som du har brug for. GTD siger at du bør starte med disse kontekster: Hjemme, kontoret, computer, telefon, ærinder og agenda. Det er helt sikkert et godt udgangspunkt men jeg vil anbefale dig at vente med at oprette en collection til din kontekst liste til at du rent faktisk har brug for den. Det er nemt at lave collections i Bujo, men man skal prøve at undgå at lave tomme collections.

Hvis du har brug for en computer til at sende en mail til din chef, og du ikke har en computer, eller den rigtige computer, i nærheden, så kan du ikke gøre det lige nu, uanset hvor vigtig den er, så er det bedre at bruge tiden på at se på din telefon liste om der er nogen opkald du lige kan få klaret, hvis du altså har din telefon på dig :-)

Det næste element vi skal se på er __projekt listen__. I din Bujo er det nemmest at have en projekt liste som et dedikeret index. Dog med den lille tilføjelse at du skal putte task bullets for hvert projekt i listen. Dette er naturligvis for at kunne identificere hvilke projekter der er færdig og hvilke der ikke er.

Mål, ansvarområder og de højere vertikale lag i GTD håndteres bedst med en __Mål og værdier collection__. Der en en super smart måde i The Bullet Journal Method. Hvis 5-4-3-2-1 metoden ikke er for dig, så kan du søge efter S.M.A.R.T metoden eller måske H.A.R.D og/eller W.O.O.P metoderne for at sætte mål.

__Someday / maybe__ listen er også vigtig. Dette laves også som en collection, og du skal naturligvis ikke lave denne før du har brug for den. Someday / maybe listen kan bruges til at parkere ting på som du gerne vil se på i fremtiden, men ikke nu. Jeg personligt bruger også listen som unfocus liste: Hvis det er på denne liste, så er det kun noget jeg bruger tid på som en del af min ugentlige gennemgang. Hvis der kommer noget op omkring et emne på min someday / maybe liste, så logger jeg det i mine daglige sider og ser på det næste gang jeg laver et ugentligt review.

Den sidste ting der er vigtigt er at definere dine indbakker til dit system. Det er de steder hvor dit GTD system modtager input. De indlysende er naturligvis dine daglige sider i din Bujo, men der er også eksterne indbakker. Du har jo nok en email adresse og du har nok også mere end en. Derudover er der også social media og besked applikationer som f.eks Slack og Telegram. Uanset hvad du har af input steder, så er det helt sikkert en god ide at definere dem på en liste. Den liste kan du bruge som tjekliste til at sørge for at du får tømt alle indbakker dagligt (eller næsten dagligt alt efter dit ambitions niveau)

# Reviews

__Dagligt review__: Dette review er bedst gjort sammen med enten am eller pm reflektionerne. Vælg den reflektion hvor du planlægger mest og put det daglige review med ind der. Hvad der indgår i dit daglige review er lidt en smagssag, men det burde som minimum indeholde at tømme alle din indbakker samt at processere det nye input. Men det kan også være en ide at gå gårsdagens gode ideer igennem og lige tjekke en gang til om det rent faktisk er gode ideer.

__Den ugentlige gennemgang__: Dette er GTD metodikkens vigtigeste element. Det er her hvor man vedligeholder sit setup og sørger for at alle data er friske og opdaterede. Det ugentlige review er delt i 3 sektioner ifølge David Allen (opfinder af GTD metodikken): Get Clear, Get current, og til sidste Get Creative. Her er hvordan jeg fortolker dem:

__Get Clear__: 
* Sørg for at alle indbakker er tømte og alt deres indhold er processeret. 
* Gå din kalender igennem 2 uger bagud og 2 uger forud for at se om der er noget der skal håndteres. 
* Lav et brain dump for at sikre at du har fået alt ud af hovedet.

__Get Current__: 
* Tjek alle context lister for om der er noget som jeg har gjort, men ikke har nået at tjekke af. 
* Gennemgå projektlisten for at se om jeg har mindst en next action for alle projekter. 
* Hvis et projekt har sin egen collection, så går jeg også lige den igennem for at sikre at jeg har fået skrevet nyt reference materiale ind i den collection.

__Get Creative__: 
* Gennemgå someday / maybe listen for at se om der der noget på listen som jeg gerne vil flytte til projektlisten eller om der er noget jeg bør putte på den liste. 
* Gennemgå mål og værdier for at se om jeg har de rigtige projekter som er nødvendige for at komme i mål.

Jeg håber dette kan hjælpe dig :-)