
---
layout: page
title: Certificeringer
comments: false
---

## Udvikler certificeringer:

* Sun Microsystems Java Programmer 1.4 og 1.6
* SAP NetWeaver EP-6.0/SP3

## Test certificeringer:

* ISTQB Advanced Techical Test Analyst
* ISTQB Foundation

## Projekt certificeringer:

* Scrum master

## Applikations certificeringer:

* Leapwork Certified Professional